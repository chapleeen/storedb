#ifndef COMMON_H
#define COMMON_H
#include <QString>

enum StatusCode
{
    OK = 0,
    ERROR,
    NOT_FOUND,
    NOT_ENOUGH
};

namespace data
{
    class Item
    {
    public:
        Item();

        Item(const QString& title, const QString& type, const uint64_t& buyPrice, const uint64_t sellPrice, const uint64_t quantity)
            : m_sTitle(title)
            , m_sType(type)
            , m_nBuyPrice(buyPrice)
            , m_nSellPrice(sellPrice)
            , m_nQuantity(quantity)
        {}

        Item(Item const& other)
        {
            this->m_sTitle     = other.m_sTitle;
            this->m_sType      = other.m_sType;
            this->m_nBuyPrice  = other.m_nBuyPrice;
            this->m_nSellPrice = other.m_nSellPrice;
            this->m_nQuantity  = other.m_nQuantity;
        }

        Item(Item&& other)
        {
            this->m_sTitle     = other.m_sTitle;
            this->m_sType      = other.m_sType;
            this->m_nBuyPrice  = other.m_nBuyPrice;
            this->m_nSellPrice = other.m_nSellPrice;
            this->m_nQuantity  = other.m_nQuantity;
        }

        Item &operator=(Item const& other)
        {
            this->m_sTitle     = other.m_sTitle;
            this->m_sType      = other.m_sType;
            this->m_nBuyPrice  = other.m_nBuyPrice;
            this->m_nSellPrice = other.m_nSellPrice;
            this->m_nQuantity  = other.m_nQuantity;

            return *this;
        }

        Item &operator=(Item && other)
        {
            this->m_sTitle     = other.m_sTitle;
            this->m_sType      = other.m_sType;
            this->m_nBuyPrice  = other.m_nBuyPrice;
            this->m_nSellPrice = other.m_nSellPrice;
            this->m_nQuantity  = other.m_nQuantity;

            return *this;
        }

        QString GetTitle() const
        {
            return m_sTitle;
        }

        void SetTitle(const QString &title)
        {
            m_sTitle = title;
        }

        QString GetType() const
        {
            return m_sType;
        }

        void SetType(const QString &type)
        {
            m_sType = type;
        }

        uint64_t GetBuyPrice() const
        {
            return m_nBuyPrice;
        }

        void SetBuyPrice(const uint64_t &buyPrice)
        {
            m_nBuyPrice = buyPrice;
        }

        uint64_t GetSellPrice() const
        {
            return m_nSellPrice;
        }

        void SetSellPrice(const uint64_t &sellPrice)
        {
            m_nSellPrice = sellPrice;
        }

        uint64_t GetQuantity() const
        {
            return m_nQuantity;
        }

        void SetQuantity(const uint64_t &quantity)
        {
            m_nQuantity = quantity;
        }

    private:
        QString m_sTitle;
        QString m_sType;
        uint64_t m_nBuyPrice;
        uint64_t m_nSellPrice;
        uint64_t m_nQuantity;
    };

}

namespace common
{
    const QString APP_DIR = "StoreDB";
    const QString DB_DIR  = "db";
    const QString DB_NAME = "db.sqlite";

    const QString DT_FORMAT = "YYYY-MM-DD HH:MM:SS";
}
#endif // COMMON_H
