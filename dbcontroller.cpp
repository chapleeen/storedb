#include "dbcontroller.h"
#include "filecontroller.h"


DBController::DBController()
{
    m_database = QSqlDatabase::addDatabase("QSQLITE");
    m_database.setDatabaseName(FileController::Instance().GetDBPath());

    if(!m_database.open())
    {
        bool opened = false;
    }

    if(!m_database.tables().contains("goods"))
    {
        m_database.exec("CREATE TABLE goods ("
                      "id INTEGER PRIMARY KEY,"
                      "title VARCHAR(100),"
                      "type VARCHAR(2),"
                      "buyprice INTEGER,"
                      "sellprice INTEGER,"
                      "quantity INTEGER)");
    }

    if(!m_database.tables().contains("transactions"))
    {
        m_database.exec("CREATE TABLE transactions ("
                      "id INTEGER,"
                      "datetime VARCHAR(20),"
                      "sum INTEGER,"
                      "invoice TEXT)");
    }

    m_database.close();
}

QString DBController::SQ(QString value)
{
    return "'" + value + "'";
}


DBController &DBController::Instance()
{
    static DBController instance;
    return instance;
}

StatusCode DBController::AddItems(const std::forward_list<std::shared_ptr<data::Item> > &dataList)
{
    m_database.open();
    for( auto item : dataList)
    {
        QSqlQuery query(m_database);
        query.exec("SELECT id, quantity, buyprice, sellprice FROM goods WHERE title = " + SQ(item->GetTitle()));
        bool bUpdated = false;
        while(query.next())
        {
            if (query.value(2).toLongLong() == item->GetBuyPrice()
                    && query.value(3).toLongLong() == item->GetSellPrice())
            {
                QString id = query.value(0).toString();
                long long quantity = query.value(1).toLongLong();
                query.exec("UPDATE goods SET quantity = " + QString::number( quantity + item->GetQuantity() ) + " WHERE id = " + id);
                bUpdated = true;
            }
        }

        if (!bUpdated)
        {
            query.prepare("INSERT INTO goods (title, type, buyprice, sellprice, quantity) "
                              "VALUES (:title, :type, :buyprice, :sellprice, :quantity)");
            query.bindValue(":title", item->GetTitle());
            query.bindValue(":type", item->GetType());
            query.bindValue(":buyprice", item->GetBuyPrice());
            query.bindValue(":sellprice", item->GetSellPrice());
            query.bindValue(":quantity", item->GetQuantity());
            query.exec();
        }
    }

    m_database.close();

    return StatusCode::OK;
}

StatusCode DBController::RemoveItems(const std::forward_list<std::shared_ptr<data::Item> > &dataList)
{
    StatusCode rc = StatusCode::OK;
    m_database.open();
    for( auto item : dataList)
    {
        rc = StatusCode::NOT_FOUND;
        QSqlQuery query(m_database);
        query.exec("SELECT id, quantity, buyprice, sellprice FROM goods WHERE title = " + SQ(item->GetTitle()));
        while(query.next())
        {
            if (query.value(2).toLongLong() == item->GetBuyPrice()
                    && query.value(3).toLongLong() == item->GetSellPrice())
            {
                QString id = query.value(0).toString();
                long long quantity = query.value(1).toLongLong();
                if ( item->GetQuantity() <= quantity )
                {
                    query.exec("UPDATE goods SET quantity = " + QString::number( quantity - item->GetQuantity() ) + " WHERE id = " + id);
                    rc = StatusCode::OK;
                }
                else
                {
                    rc = StatusCode::NOT_ENOUGH;
                    break;
                }
            }
        }

        if (rc != StatusCode::OK)
        {
            break;
        }
    }

    m_database.close();

    return rc;
}

StatusCode DBController::GetItems(const QString &request, std::forward_list<std::shared_ptr<data::Item> > &dataList)
{

    return StatusCode::OK;
}
