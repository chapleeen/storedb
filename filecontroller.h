#ifndef FILECONTROLLER_H
#define FILECONTROLLER_H
#include <QString>


class FileController
{
public:
    static FileController& Instance();
    QString GetDBPath();

private:
    QString m_sDBPath;

    FileController();
    FileController(FileController const&) = delete;
    FileController(FileController&&) = delete;
    FileController &operator=(FileController const&) = delete;
    FileController &operator=(FileController &&) = delete;
};

#endif // FILECONTROLLER_H
