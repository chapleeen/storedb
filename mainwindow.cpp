#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dbcontroller.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_sellBtn_clicked()
{
    std::forward_list<std::shared_ptr<data::Item>> someItems;
    someItems.push_front(std::make_shared<data::Item>(data::Item("Продукт2", "шт", 4000, 6500, 10)));
    someItems.push_front(std::make_shared<data::Item>(data::Item("Продукт1", "шт", 17000, 22000, 10)));

    DBController::Instance().RemoveItems(someItems);
}

void MainWindow::on_addGoodsBtn_clicked()
{
    std::forward_list<std::shared_ptr<data::Item>> someItems;

    someItems.push_front(std::make_shared<data::Item>(data::Item("Продукт1", "кг", 17000, 22000, 7)));
    someItems.push_front(std::make_shared<data::Item>(data::Item("Продукт2", "шт", 4000, 6500, 30)));
    someItems.push_front(std::make_shared<data::Item>(data::Item("Продукт3", "шт", 30000, 35000, 2)));

    DBController::Instance().AddItems(someItems);
}
