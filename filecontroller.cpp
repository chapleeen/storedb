#include "filecontroller.h"
#include <QProcessEnvironment>
#include "common.h"
#include <QDir>
#include <QFile>

FileController::FileController()
{
    QString sDBDir = QProcessEnvironment::systemEnvironment().value("APPDATA") + QDir::separator() + common::APP_DIR + QDir::separator() + common::DB_DIR;
    const QDir appDir(sDBDir);
    if (!appDir.exists())
    {
        if(!appDir.mkpath(sDBDir))
        {
            throw std::exception("Unable to create application dir");
        }
    }
    m_sDBPath = sDBDir + QDir::separator() + common::DB_NAME;

    QFile dbFile(m_sDBPath);
    if(!dbFile.exists())
    {
        if(!dbFile.open(QIODevice::ReadWrite))
        {
            throw std::exception("Unable to create db file");
        }
        dbFile.close();
    }
}

FileController& FileController::Instance()
{
    static FileController instance;
    return instance;
}

QString FileController::GetDBPath()
{
    return m_sDBPath;
}
